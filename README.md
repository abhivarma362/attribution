## Aim
To let the user define their own metrics using the available columns and compute metrics.

Metrics can be raw / derived or advanced metrics.

## Input
Input : Delta tables / CSV / Parquet file

Flow diagram for metrics aggregation

 

## Workflow
There are 2 main parts in the attribution code:

**Configs** : These are of dictionary data type which defines the tables and its parameters that we are going to extract. There are 3 types of configs:

`get_events_config` :

Used to derive tables from the main events table  

   Code Snippets


<pre>'stylingclick' : {
   'filters' : {
      'in':{
      'name' : 'stylingClick', 
               'action' : 'click'
               } , 
         'notNull' : ['module_id']
                  },
    'columns_to_extracted' : {'json_field' : 'metadata',
                              'columns_to_be_added': ['page_id']
                   }
  },
  </pre>
 

`attribution_config` : 

* Used to get the attribution (buy and addToCart) related metrics which is derived from the buys table and atc table in get_events_config.

Code snippets:


  <pre>'buys' : {
                    'attribution_event' :'buys', # data from get_events_config
                    'attribution_type' :  ['last','first', 'assisted'], # Direct, Indirect
                    'lookback_period' : [0,1,7] # days, blank if assisted  
                    # To-do --> Session level direct attr
                    },
  'atc' : {         
                    'attribution_event' :'atc', # data from get_events_config
                    'attribution_type' : ['first','last','assisted'], # First, Last or Multi, blank if assisted
                    'lookback_period' : [0,1,7] # days, blank if assisted
                    }
}</pre>
 

`metrics_config` : 

* Used to compute all the metrics needed. It has dependencies on get_events config.

     Code snippets:


<pre>'outfits_tried' : {
              'computation' : F.countDistinct,
              'field_name' : 'outfit_key',
              'level' : 'strategy_id',
              'table' : get_events_config['outfitstried']
            }</pre>
 

 `client_id_config` : 

 * Used for metrics computation

Code Snippets 


  <pre>client_id_config = {
  'client_details' :  client_details,
  'events_config' : get_events_config,
  'attribution_config' : attribution_config,
  'metrics_config' : metrics_config
  }</pre>

 

Functions
`get_tables()`:

* Input : min_date , max_date , client_name and client_id 

* Output: Returns input tables like events table and products table.

* Use case : To retrieve events table and products table from s3 bucket. 

Example : 


`events_table ,products_table = get_tables(min_date,max_date,client_name,client_id)`

`get_hierarchy_level()`

* Input : input_column as string and hierarchy as list

* Output: Returns the hierarchy of a particular level.

* Use case : To define the hierarchy needed for a particular metric

Example:

``## Define the hierarchy as a list , let's say l is the list
get_hierarchy_level('journey_id',l)``
 

`get_events_filters() `

* Input: event_filter_config as a dictionary

* Output: Returns an output list of all the filters from the events_config

* Use case : To extract the filter conditions for the events data table 

* Example:


```get_event_filters(events_config['filter']) ## Events config is the first layer of the get_events_config```
 

`get_extracted_columns()`:

* Input : event_extract_config as a list

* Output : Returns a dictionary of columns extracted from the event_extract_config

* Use case : To extract columns such as order_id, outfit_key etc.. from the event_extract_config. Also capable of extracting columns from nested json.

Example:


```get_extracted_columns(event_config['columns_to_extract'])```
 

`deduplicate_events()` : 

* Input : event_dedup_config as a dictionary

* Output : Returns the windowSpec  for a window function

* Use case : To dedup the tables which needs dedup condition

 Example:

```deduplicate_events(event_config['dedup'])```

`grouped_filter()`:

* Input : a spark dataframe , group_filter_config as a list

* Output : Returns the filtered columns as a dictionary

* Use case : To evaluate the having conditions for the events data table

Example: 


```grouped_filter(dataframe, events_config['having'])```
 

`product_join()`:

* Input : Main table to join, products table , left_on condition , right_on condition , products_columns parameter (optional , default is None)

* Output  : Returns the processed data after the use case mentioned below.

* Use case : To join the any table with product table.

Example:


```product_join(events_data, products_data, left_on, right_on, products_columns = None)``` 
```## default products columns needed are already defined in the function , parameter is for additional columns```

`get_events_data()`:

* Input : data(events data), products data, min_date, max_date, env(environment) and event_config(table to be used).

* Output : Return the filtered data after evaluating all necessary conditions.

* Use case : This will basically return the data needed for the metrics computations.

Example:


```get_events_data(data, products_data, min_date, max_date, env, event_config['buys'])```
 

`get_attribution_condition()`:

* Input : attribution_config as a dictionary.

* Output : Returns the set of attribution rules.

* Use case : This function will return the attribution rules needed for the buys attribution and add to cart attribution.

Example:


```get_attribution_condition(attribution_config['buys'])```


`get_attribution_table()`:

* Input : events data, products data, min_date, max_date, env(environment) and attribution_rule(set of attribution conditions).

* Output : Returns the attribution table.

* Use case : This function returns the attribution table based on the attribution rule we got from the get_attribution_condition().

Example:


```get_attribution_table(events_data, products_data, min_date, max_date, env, attribution_rule)```
 

`metrics_computation()`:

* Input : events data, products data, min_date, max_date, env(environment) and client_id_config (contains set of all configs used).

* Output : Returns the final metrics data.

* Use case : This function computes all the metrics we defined in the metrics_config and aggregates and returns a table with all the levels(metric specific), metric_name and metric_value.

Example:


```metrics_computation(events_data, products_data, min_date, max_date, env, client_id_config)```
 

`get_output_delta()`:

* Input : df ( Final metrics data).

* Output : Nothing will return

* Use case : Writes delta table to a specific key in a specific s3 bucket.

Example:


```get_output_delta(df)```